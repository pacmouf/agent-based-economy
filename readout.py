import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

# Classes used for simulation
class Period(object):
    """Tracks period variables and makes period reports"""
    
    def __init__(self,year):
        """Variables measured in period"""
        self.stone_produced = 00
        self.food_produced = 00
        self.production_value = 00
        self.food_consumed = 00
        self.stone_consumed = 00
        self.consumed_value = 00
        self.satiated = 00
        self.dead = 00
        self.population = 00
        self.year = year
    
    def write_output(self, year, output):
        """Writes variables to file"""
        output.write("Period " + str(self.year) + " Summary")
        output.write("\n")
        output.write("Stone produced in period " + str(self.year) + ": " + 
str(self.stone_produced))
        output.write("\n")
        output.write("Food produced in period " + str(self.year) + ": " + 
str(self.food_produced))
        output.write("\n")
        output.write("$" + str(self.production_value) + " of goods were \
produced in period " + str(self.year))
        output.write("\n")
        output.write("\n")
        output.write("Stone consumed in period " + str(self.year) + ": " + 
str(self.stone_consumed))
        output.write("\n")
        output.write("Food consumed in period " + str(self.year) + ": " + 
str(self.food_consumed))
        output.write("\n")
        output.write("$" + str(self.consumed_value) + " of goods were \
consumed in period " + str(self.year))
        output.write("\n")
        output.write("\n")
        output.write(str(self.satiated) + " agents had enough food and \
shelter in period " + str(self.year))
        output.write("\n")
        output.write(str(self.dead) + " agents perished due to a lack of food \
or shelter in period " + str(self.year))
        output.write("\n")
        output.write("\n")
        output.write("Population at end of period " + str(self.year) + " is \
" + str(self.population))
        output.write("\n")
        output.write("\n")
        
class Simulation(object):
    """Manages aggregate variables of the simulation"""
    def __init__(self):
        """Variables tracked in simulation"""
        self.stone_produced = 00
        self.food_produced = 00
        self.production_value = 00
        self.food_consumed = 00
        self.stone_consumed = 00
        self.consumed_value = 00
        self.satiated = 00
        self.dead = 00
        self.population = 00 

    def write_output(self, output):
        """Prints variables to file"""
        output.write("\n")
        output.write("Aggregate Summary of Simulation")
        output.write("\n")
        output.write("Aggregate value of goods produced: $" + 
str(self.production_value))
        output.write("\n")
        output.write("Aggregate stone produced: " + str(self.stone_produced))
        output.write("\n")
        output.write("Aggregate food produced: " + str(self.food_produced))
        output.write("\n")
        output.write("Aggregate stone consumed: " + str(self.stone_consumed))
        output.write("\n")
        output.write("Aggregate food consumed: " + str(self.food_consumed))
        output.write("\n")
        output.write("Aggregate value of goods consumed: $" + 
str(self.consumed_value))
        output.write("\n")
        output.write(str(self.dead) + " agents died from the running of this \
simulation")
        output.write("\n")
        output.write("Simulation finished with " + 
str(self.population) + " agents.")
        output.write("\n")
        output.write("\n")

        
def graph_output(sim_duration, period_object):
    """Function for outputing graphical data about simulation"""
    # Plots period against production value
    plt.figure(1)
    # Empty list for putting data of interest in
    data_one = []
    data_two = []
    # Goes through the period objects (since each period is an object with
    # various attributes), pulls the production value from that period
    # and adds them to the production data list
    for period in sim_duration:
        data_one.append(period_object[period].production_value)
        data_two.append(period_object[period].consumed_value)            
    # Plots sim_duration (which is a list from 0 => user input) against 
    # production data for that period
    plt.plot(sim_duration, data_one, 'b-', 
             sim_duration, data_two, 'r-')
    label_a = mpatches.Patch(color = 'blue', label = 'Production Value')
    label_b = mpatches.Patch(color = 'red', label = 'Consumption Value')
    plt.legend(handles = [label_a, label_b] )
    plt.xlabel('Period')
    plt.ylabel('Value ($)')
    top = 0
    if max(data_one) > max(data_two):
        top = max(data_one)
    elif max(data_one) < max(data_two):
        top = max(data_two)
    else:
        top = max(data_one)
    top = top + 0.1*top    
    plt.axis([0, max(sim_duration), 0, top])
    plt.title('Production and Consumption Value over Time')
    plt.grid(b = True, which = 'major', color = 'k', linestyle = '-')
    plt.grid(b = True, which = 'minor', color = 'k', linestyle = '--')
    #Plots period against stone and food production
    plt.figure(2)
    # Empties data list (since it has already been plotted)
    data_one = []
    data_two = []
    for period in sim_duration:
        data_one.append(period_object[period].stone_produced)
        data_two.append(period_object[period].food_produced)
    plt.plot(sim_duration, data_one, 'b-',
             sim_duration, data_two, 'r-')
    label_a = mpatches.Patch(color = 'blue', label = 'Stone Produced')
    label_b = mpatches.Patch(color = 'red', label = 'Food Produced')
    plt.legend(handles = [label_a, label_b] )
    plt.xlabel('Period')
    plt.ylabel('Units Produced')
    top = 0
    if max(data_one) > max(data_two):
        top = max(data_one)
    elif max(data_one) < max(data_two):
        top = max(data_two)
    else:
        top = max(data_one)
    plt.axis([0, max(sim_duration) , 0, top + 0.1*top])
    plt.title('Production over Time')
    plt.grid(b = True, which = 'major', color = 'k', linestyle = '-')
    plt.grid(b = True, which = 'minor', color = 'k', linestyle = '--')
    # Plots Consumption of Different items against one another
    plt.figure(3)
    data_one = []
    data_two = []
    for period in sim_duration:
        data_one.append(period_object[period].stone_consumed)
        data_two.append(period_object[period].food_consumed)
    plt.plot(sim_duration, data_one, 'b-',
             sim_duration, data_two, 'r-')
    label_a = mpatches.Patch(color = 'blue', label = 'Stone Consumed')
    label_b = mpatches.Patch(color = 'red', label = 'Food Consumed')
    plt.legend(handles = [label_a, label_b] )
    plt.xlabel('Period')
    plt.ylabel('Units Consumed')
    top = 0
    if max(data_one) > max(data_two):
        top = max(data_one)
    elif max(data_one) < max(data_two):
        top = max(data_two)
    else:
        top = max(data_one)
    plt.axis([0, max(sim_duration) , 0, top + 0.1*top])
    plt.title('Consumption over Time')
    plt.grid(b = True, which = 'major', color = 'k', linestyle = '-')
    plt.grid(b = True, which = 'minor', color = 'k', linestyle = '--')
    #Plots period against population
    plt.figure(4)
    data = []
    for period in sim_duration:
        population = period_object[period].population
        data.append(population)
    plt.plot(sim_duration, data)
    plt.xlabel('Period')
    plt.ylabel('Population')
    top = max(data)
    top = top + 0.1*top    
    plt.axis([0, max(sim_duration), 0, top])
    plt.title('Population over Time')
    plt.grid(b = True, which = 'major', color = 'k', linestyle = '-')
    plt.grid(b = True, which = 'minor', color = 'k', linestyle = '--')
    # Shows the plot
    plt.show()
    
import commodity
from random import randrange

class Agent(object):
    """Agent object with demand, property, ability to produce, sell, buy and 
    consume, financial assets and count how much of items they have 
    """
    def __init__(self, name):
        """__init___ that gives agent a name, empty property, zero assets and 
        liabilities.
        """
        self.name = name
        self.property = [] 
        self.assets = 0.00
        self.liabilities = 0.00
        # Lists for the agent to track there own consumption. Each item
        # in the list corresponds to a period's consumption, so 
        # it can be called by self.food_consumption[period]
        self.food_consumed = []
        self.shelter_consumed = []
        # I would like to eventually turn both productivity and demand into
        # objects so they can have multiple attributes (potentially with 
        # variance between agents)
        self.demand = {
            'food' : 0,
            'shelter' : 0
        }
        self.productivity = {
            'agriculture' : 4,
            'mining' : 2 
        }
        self.marked_for_death = False
        self.food_demand_growth = 2
        self.shelter_demand_growth = 1
        self.hunger_limit = 8 #+ randrange(3)
        self.exposure_limit = 4 #+ randrange(3)
 
    def demand_grow(self):
        """Demand for necessaries increase"""
        self.demand['food'] = self.demand['food'] + self.food_demand_growth
        self.demand['shelter'] = self.demand['shelter'] \
        + self.shelter_demand_growth
        
    def consume(self, periods, date):
        """Agents attempt to consume commodities they own to satisfy demand.
        If they do not have the necessary commodities, they are marked for 
        death and will later die when the code checks if they are marked. 
        Items are removed when they are consumed 
        """
        self.inventory_count()
        self.food_eaten = 0
        self.shelter_usage = 0
        self.consumeditems = []
        # Goes through property consuming different items. Agents notes once 
        # it has consumed good. Consuming reduces demand.
        # Shelter is consumed once used. For some reason
        for item in self.property:
            if self.demand['food'] > 0:
                if item.group == "food":
                    self.food_eaten = self.food_eaten + 1
                    self.demand['food'] = self.demand['food'] - 1
                    # depreciates the good
                    item.depreciate()
                    # if the good has been finished, removes it from inventory
                    if item.condition == 0: 
                        self.consumeditems.append(item) 
                    periods[date].food_consumed = \
                    periods[date].food_consumed + 1
                    periods[date].consumed_value = \
                    item.price + periods[date].consumed_value
        for item in self.property:
            if self.demand['shelter'] > 0:
                if item.group == 'shelter':
                    self.shelter_usage = self.shelter_usage + 1
                    self.demand['shelter'] = self.demand['shelter'] - 1
                    item.depreciate()
                    if item.condition == 0:
                        self.consumeditems.append(item)
                    periods[date].stone_consumed = \
                    periods[date].stone_consumed + 1
                    periods[date].consumed_value = \
                    item.price + periods[date].consumed_value
        for item in self.consumeditems:
            self.property.remove(item)
        # Appends consumption to tracking list. As agents consume once/period
        # it will track consumption/period
        self.food_consumed.append(self.food_eaten)
        self.shelter_consumed.append(self.shelter_usage)   

    def produce(self, periods, date):
        """Agents attempt to produce depending on the results of last periods 
        consumption. If it is the first period, they randomly select what 
        good to produce. If they were satiated last period (i.e. food and 
        shelter demand dropped by x(2) and (y)1 respectively), then they 
        make the same as last time. Otherwise, they swap production to the 
        good they were lacking. With any luck, it should tend towards the given 
        balance of stone versus food production.
        """
        # I think there is a way better way of doing this, but right now I
        # want to test the concept of production changing
        # This will not work for dynamic product quantities (beyond two)
        if date == 0:
            # When you give users ability to weight population dis, affect 
            # the random range and/or the number that generate users.
            # that way you can make pseudo percentages
            self.production_number = randrange(1)
            if self.production_number <= 0:
                self.produce_commodity(periods, date, 'agriculture', 'food')
                self.production_number = 0
            elif self.production_number > 0:
                self.produce_commodity(periods, date, 'mining', 'shelter')
                self.production_number = 1
        else:    
            if self.food_consumed[date -1] == 0:
                self.produce_commodity(periods, date, 'agriculture', 'food')
                self.production_number = 0
            elif self.shelter_consumed[date - 1] == 0:
                self.produce_commodity(periods, date, 'mining', 'shelter')
                self.production_number = 1
            elif self.production_number == 0:
                self.produce_commodity(periods, date, 'agriculture', 'food')
                self.production_number = 0
            elif self.production_number == 1:
                self.produce_commodity(periods, date, 'mining', 'shelter')
                self.production_number = 1
        self.inventory_count()

    def produce_commodity(self, periods, date, industry, item_type):
        """Method for producing commodities"""
        count = 0
        while count < self.productivity[industry]:
                    good = commodity.Commodity(self, 100.00/\
                                                self.productivity\
                                                [industry], item_type)
                    self.property.append(good)
                    count = count + 1
                    # Can make this better too
                    if industry == 'agriculture':
                        periods[date].food_produced = \
                        periods[date].food_produced + 1
                        periods[date].production_value = \
                        periods[date].production_value + good.price
                    elif industry == 'mining':
                        periods[date].stone_produced = \
                        periods[date].stone_produced + 1  
                        periods[date].production_value = \
                        periods[date].production_value + good.price

    def sell_offer(self, market):
        """ Agent add sell offers to t  he market based on surplus productivity
        """
        self.offer = []
        # Figures out how large the surplus is
        self.food_sale_surplus = self.food_count - self.food_demand_growth
        self.stone_sale_surplus = self.shelter_count - self.shelter_demand_growth

        for item in self.property: 
            if self.food_sale_surplus > 0:
                if item.group == "food":
                    self.offer.append(item)
                    self.food_sale_surplus - 1
            if self.stone_sale_surplus > 0:
                if item.group == "shelter":
                    self.offer.append(item)
                    self.stone_sale_surplus - 1
        for item in self.offer:
            self.property.remove(item)
        market.add_offers(self.offer)   

    def buy(self, market):
        """Agent checks market sell offers for needs (commodities it lacks)
        and buys first item it sees that fulfils this criteria.
        """
        self.inventory_count()
        self.food_shopping_list = self.demand['food'] - self.food_count
        self.shelter_shopping_list = self.demand['shelter'] - self.food_count
        self.bought_commodities = []
        # If agent lacks food, looks for food        
        for item in market.marketboard:
            if self.food_shopping_list > 0:
                if item.group == "food":
                    # Pays for asset by subtracting money from account
                    self.assets = self.assets - item.price
                    # Credits money to sellers account
                    item.owner.assets = \
                    item.owner.assets + item.price
                    # Adds the property to agent's property
                    self.property.append(item)
                    # Changes name on commodity
                    self.property[-1].owner = self
                    # Adds item to list that tracks how many commodities 
                    # are bought
                    self.bought_commodities.append(item)
                    # Agent registers the fact they just got some food
                    self.food_shopping_list = self.food_shopping_list - 1         
        for item in market.marketboard:
            if self.shelter_shopping_list > 0:
                if item.group == "shelter":
                    self.assets = self.assets - item.price
                    item.owner.assets = \
                    item.owner.assets + item.price
                    self.property.append(item)
                    self.property[-1].owner = self
                    self.bought_commodities.append(item)
                    self.shelter_shopping_list = self.shelter_shopping_list \
                                                - 1    
        for item in self.bought_commodities:
            market.marketboard.remove(item)           
 
    def inventory_count(self):
        """Method where agent goes through property it owns and makes a count
        of how much food or shelter the agent owns.
        """
        self.food_count = 0
        self.shelter_count = 0
        for item in self.property:
            if item.group == "food":
                self.food_count = self.food_count + 1
            elif item.group == "shelter":
                self.shelter_count = self.shelter_count + 1
                
    def demand_check(self, periods, date):
        """Checks agent's consumption against deprivation limits"""
        if self.demand['food'] == 0 \
        and self.demand['shelter'] == 0:
            periods[date].satiated = periods[date].satiated + 1
        elif self.demand['food'] >= (self.hunger_limit/2) \
        and self.demand['shelter'] >= (self.exposure_limit/2):
            periods[date].dead = periods[date].dead + 1
            self.marked_for_death = True
        elif self.demand['food'] > self.hunger_limit:
            periods[date].dead = periods[date].dead + 1
            self.marked_for_death = True
        elif self.demand['shelter'] > self.exposure_limit:
            periods[date].dead = periods[date].dead + 1
            self.marked_for_death = True
            
    def reclaim_goods(self, market):
        """Agents take back unsold item"""
        reclaimed_goods = []
        for item in market.marketboard:
            if item.owner == self:
                self.property.append(item)
                reclaimed_goods.append(item)
        for item in reclaimed_goods:
            market.marketboard.remove(item)
                
#    def inventory_search(self, item):
#        """Method for agents to search their inventory"""
#        for item in self.inventory:
#            if item.group == item:
#                return TRUE
#            elif item.group =/=item:
#                return FALSE
    
#    def inventory_group(self):
#        """Method for agents to group their inventory"""
    
#    def inventory_sort(self):
#        """Method for agents to sort their inventory"""

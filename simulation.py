# Import matplotib
import readout
import market
import agent


# Code to run simulation

print "Welcome to Willem's Agent Based Economy"

# User decides if they want 

# User decides how many agents are in the economy
population = raw_input("How many agents in the economy? ")

# User decides how long the simulation will run
duration = raw_input("How many periods should the simulation run? ")

# Checks if input is an integer
try:
    val = int(duration)
except ValueError:
    print "That was not a int! Please enter a number"
    duration = raw_input("How many periods should the simulation run? ")

# User names output file
filename = raw_input("What would you like to call the output file? ")

# User decides if they want graphs
graphs_please = raw_input("Do you want a graphical output of results? (Y/N) ")
graphs_please = graphs_please.lower()

while graphs_please not in {'yes', 'no', 'y', 'n'}:
    graphs_please = raw_input("Yes or no only. Please try again. ")
    graphs_please = graphs_please.lower()
    
if graphs_please in {'yes', 'y'}:
    graphs_please = True
else:
    graphs_please = False

# Sets up file for writing summary
output = open(filename + '.txt', 'w')
line1 = "Macroeconomic Summary for " + str(filename)
output.write(line1)
output.write("\n")
output.write("\n")

population = int(population)
duration = int(duration)
period = range(duration)
population_range = range(population)

# Generates recording periods based on previous input
periods = {}
for number in period:
    periods[number] = readout.Period(number)

# Makes a market
market = market.Market()
    
#Makes an aggregate tracker
simulation = readout.Simulation()

date = 0

def agent_model(population_range, period, population, output):
    """Function that runs the simulation"""

    # Generates agents based on population
    agents = []
    for person in population_range:
        actor = agent.Agent('Agent ' + str(person))
        agents.append(actor)
    
    # Writes initial population summary
    output.write("Economy starts with total population of " +
str(population) + ".")
    output.write("\n")
    output.write("\n")
    
    # Runs economy
    for year in period:
        # THIS IS A DIRTY HACK. CHECK WITH BIGGLES HOW ON EARTH TO MAKE 
        # THIS WORK. The issue is that the periods[year] wants a global value
        # because the class is outside this function, but I want it to looks
        # at the local year that this loop is iterating through 
        global date
        date = year
        # Makes a range so that I can iterate along list of agents. Does it
        # at the start of each year because people might have died
        pop_list= len(agents)
        pop_list = range(pop_list)
        #Increases demand due to new period
        for person in pop_list:
            agents[person].demand_grow()

        # Makes the populace try to produce
        for person in pop_list:
            agents[person].produce(periods, date)
 
        # Populace puts their items up for sale
        for person in pop_list:
            agents[person].sell_offer(market)
  
        # Populace buys items
        for person in pop_list:
            agents[person].buy(market)

        # Agent takes back unsold goods
        for person in pop_list:
            agents[person].reclaim_goods(market)
         
        # Makes the populace try to consume
        for person in pop_list:
            agents[person].consume(periods, date)
        
        # Checks how satiated/dead agent is
        for person in pop_list:
            agents[person].demand_check(periods, date)

        # IT'S MURDERING TIME 
        death_list = []
        for person in pop_list:
            if agents[person].marked_for_death:
                death_list.append(agents[person])
        for person in death_list:
            agents.remove(person)
        
        periods[year].population = len(agents)
        # Writes out details of period to summary
        periods[year].write_output(year, output)
        
        # Adds to aggregate details
        simulation.stone_produced = \
        simulation.stone_produced + periods[year].stone_produced
        simulation.food_produced = \
        simulation.food_produced + periods[year].food_produced
        simulation.production_value = \
        simulation.production_value + periods[year].production_value
        simulation.stone_consumed = \
        simulation.stone_consumed + periods[year].stone_consumed
        simulation.food_consumed = \
        simulation.food_consumed + periods[year].food_consumed
        simulation.consumed_value = \
        simulation.consumed_value + periods[year].consumed_value
        simulation.dead = simulation.dead + periods[year].dead
        simulation.population = periods[year].population
        
    # Writes Out Aggregate Output
    simulation.write_output(output)

    # Closes Output File
    output.close()

    # Outputs graphical representation of data
    if graphs_please:
        readout.graph_output(period, periods)

# Simulation
agent_model(population_range, period, population, output)

Hello,

This is my ongoing project to develop  an agent based capitalist economy. It is also my excuse to learn how to code. 

Right now, it is extremely simple. However, I have high hopes that the farmers and miners today will learn how to exploit each other in the future.

If you want to try it, download the files, and then run simulation.py with python. Follow instructions from there. You may get graphs.

Currently I am working on improving the sophistication of agents' ability to choose prices and buy products.
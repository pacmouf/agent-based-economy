class Commodity(object):
    """Commodity class that manages the dimensions of a commodity. In this 
    case it includes the owner, the price and the group (food or stone) of
    the commodity.
    """
    def __init__(self, owner, price, group):
        """__init__  that makes commodities have owners, price and grouping 
        based on object-input.
        """
        self.owner = owner
        self.price = price
        self.group = group
        # deprecation is a number between 0 and 1 
        self.depreciation = {
            'food' : 1,
            'shelter' : 0.1
        }
        # condition is a number between 0 and 100. New commodity is 100
        self.condition = 100

    def depreciate(self):
        """method that depreciates the commodity's condition based on its 
        deprecation rate.
        """
        self.condition = self.condition - (100 * \
        self.depreciation[self.group])
    
class Bank(object):
    """Class for the creation of a bank that makes loans and holds savings. 
    Currently, assets and liabilities are held privately so this class is 
    not used
    """
    def __init__(self):
        """def that generates the assets and liabilities of the bank"""
        self.assets = 0
        self.liabilities = 0
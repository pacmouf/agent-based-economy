class Market(object):
    """Market class that is the 'space' that buyers and seller come to list 
    sell offers and buy goods from the sell offers.
    """
    def __init__(self):
        """__init__ that creates the marketboard as a list for the listing of 
        sell offers.
        """
        self.marketboard = []
        
    def add_offers(self, sell_offer):
        """Method that enables sell_offers by agents to be added to the market."""
        self.marketboard.extend(sell_offer)